from ui_halma import get_menu_input, help_sequence, first_move_info, \
                     print_board, get_command, print_moves, print_stuck, \
                     print_command_fail, print_computer_turn, x_dictionary, \
                     print_computer_decision, checkpoint, print_winner, \
                     computer_checkpoint, print_menu_screen, \
                     print_menu_input_fail, no_moves
from bot_halma import generate_move
from class_halma import Board
from random import randint
from typing import Optional


def process_command(choice: str) -> list[tuple[int, int]]:
    """
    Processes the command passed during a player's turn.
    Possible outputs:
    [(-2, -2)] - invalid output
    [(-1, -1)] - quit
    [(0, -1)] - help
    [position] - check available moves
    [position1, position2] - perform a move
    """
    if choice == "quit":
        return [(-1, -1)]
    if choice == "help":
        return [(0, -1)]
    parts = choice.split()
    if len(parts) not in [1, 2]:
        return [(-2, -2)]
    result = []
    for coordinate in parts:
        if (
            len(coordinate) not in [2, 3]
            or coordinate[0] not in x_dictionary().keys()
        ):
            return [(-2, -2)]
        x = x_dictionary()[coordinate[0]]
        try:
            y = int(coordinate[1:])
        except ValueError:
            return [(-2, -2)]
        if y not in x_dictionary().values():
            return [(-2, -2)]
        result.append((x, y))
    return result


def player_turn(board: Board, player: int) -> Optional[int]:
    """
    Handles the player's turn.
    """
    print_board(board)
    finished_turn = False
    while not finished_turn:
        choice = get_command(player)
        choice = process_command(choice)
        if choice == [(-2, -2)]:  # invalid input
            print_command_fail(0)
        elif choice == [(-1, -1)]:  # quit
            finished_turn = True
            return -1
        elif choice == [(0, -1)]:  # help
            help_sequence()
            checkpoint()
            print_board(board)
        else:
            location = choice[0]
            moves = board.check_possible_moves(location)
            allowed = board.remove_moves_out_of_camp(player, location, moves)
            if len(choice) == 1:  # check legal moves
                print_moves(moves)
            else:  # make a move
                pawn = board.field_content(location)
                destination = choice[1]
                if not pawn:
                    print_command_fail(1)
                elif pawn.owner() != player:
                    print_command_fail(2)
                elif destination not in moves:
                    print_command_fail(3)
                elif destination not in allowed:
                    print_command_fail(4)
                else:
                    finished_turn = True
                    board.move_pawn(location, destination)
    return board.check_winning_position()


def computer_turn(board: Board, player: int) -> None:
    """
    Handles the computer's turn.
    """
    print_board(board)
    print_computer_turn()
    move = generate_move(board, player)
    if move is None:
        no_moves()
    else:
        location, destination = move
        board.move_pawn(location, destination)
        print_computer_decision([location, destination])
    quit = computer_checkpoint()
    if quit:
        return -1
    return board.check_winning_position()


def play_game(player1_is_human: bool, player2_is_human: bool) -> None:
    """
    Starts a new game, where player1 plays with whites - 88
    and has the first move, and player2 plays with blacks - [].
    """
    if player1_is_human ^ player2_is_human:
        first_move_info(player1_is_human)
        checkpoint()
    board = Board()
    player = 1
    result = None
    while not result:
        human = player1_is_human if player == 1 else player2_is_human
        if human:
            result = player_turn(board, player)
        else:
            result = computer_turn(board, player)
        player = 2 if player == 1 else 1
        if not result:
            stuck = board.check_if_stuck()
            if stuck:
                result = 2 if stuck == 1 else 1
                print_stuck(result)
                checkpoint()
    if result != -1:
        print_board(board)
        print_winner(result)
        checkpoint()


def main():
    playing = True
    while playing:
        print_menu_screen()
        valid_option = False
        while not valid_option:
            choice = get_menu_input()
            if choice in ["1", "2", "3", "4", "5"]:
                choice = int(choice)
                valid_option = True
            else:
                print_menu_input_fail()
        if choice == 1:  # player vs computer
            if randint(0, 1) == 0:
                play_game(True, False)
            else:
                play_game(False, True)
        elif choice == 2:  # player vs player
            play_game(True, True)
        elif choice == 3:  # computer vs computer
            play_game(False, False)
        elif choice == 4:  # help
            help_sequence()
            checkpoint()
        else:  # quit
            playing = False


if __name__ == "__main__":
    main()

from bot_halma import position_quality, calculate_differences, \
                      distance_pythagorean, distance_city_block, \
                      find_closest_pawn, player_pawns, find_lowest_value, \
                      order_pawns_by_distance, find_all_moves, find_highways, \
                      find_best_move, quality_after_move, is_highway, \
                      generate_move_towards, find_empty_fields, \
                      verify_highway_move, find_move_to_highway, \
                      pawns_to_pair_up, pair_up_valid, pair_up_lone, \
                      generate_move
from class_halma import Board, Pawn, black_camp, white_camp
import pytest


def test_position_quality_starting():
    board = Board()
    quality = position_quality(board, 1)
    assert quality > 400
    quality = position_quality(board, 2)
    assert quality > 400


def test_position_quality_two_jumps():
    board = Board()
    original = position_quality(board, 1)
    board.move_pawn((14, 2), (12, 4))
    assert position_quality(board, 1) == original - 4
    board.move_pawn((13, 3), (11, 5))
    assert position_quality(board, 1) == original - 8


def test_position_quality_diagonal_moves():
    board = Board()
    original = position_quality(board, 1)
    board.move_pawn((13, 3), (12, 4))
    assert position_quality(board, 1) == original - 2
    board.move_pawn((12, 4), (11, 5))
    assert position_quality(board, 1) == original - 4
    board.move_pawn((11, 5), (10, 6))
    assert position_quality(board, 1) == original - 6
    board.move_pawn((10, 6), (9, 7))
    assert position_quality(board, 1) == original - 8


def test_position_quality_winning():
    board = Board()
    pawns = board.pawns()
    camp = black_camp()
    for i in range(0, 19):
        pawns[i].set_position(camp[i])
    for i in range(19, 30):
        pawns[i].set_position((10, i-18))
    for i in range(30, 38):
        pawns[i].set_position((9, i-29))
    quality = position_quality(board, 1)
    assert quality == 0
    quality = position_quality(board, 2)
    assert quality > 160


def test_calculate_differences_normal():
    dist_x, dist_y = calculate_differences((4, 9), (1, 5))
    assert dist_x == 3
    assert dist_y == 4


def test_calculate_differences_vertical():
    dist_x, dist_y = calculate_differences((1, 5), (1, 9))
    assert dist_x == 0
    assert dist_y == 4


def test_calculate_differences_horizontal():
    dist_x, dist_y = calculate_differences((4, 5), (1, 5))
    assert dist_x == 3
    assert dist_y == 0


def test_calculate_differences_zero():
    dist_x, dist_y = calculate_differences((1, 5), (1, 5))
    assert dist_x == 0
    assert dist_y == 0


def test_distance_pythagorean_normal():
    correct_distance = 10**0.5
    distance = distance_pythagorean((4, 6), (1, 5))
    assert distance == pytest.approx(correct_distance)


def test_distance_pythagorean_zero():
    distance = distance_pythagorean((2, 2), (2, 2))
    assert distance == pytest.approx(0)


def test_distance_city_block_normal():
    distance = distance_city_block((4, 6), (1, 5))
    assert distance == 4


def test_distance_city_block_zero():
    distance = distance_city_block((2, 2), (2, 2))
    assert distance == 0


def test_player_pawns_white():
    board = Board()
    pawns = player_pawns(board, 1)
    assert len(pawns) == 19
    assert pawns[0].owner() == 1


def test_player_pawns_black():
    board = Board()
    pawns = player_pawns(board, 2)
    assert len(pawns) == 19
    assert pawns[0].owner() == 2


def test_find_closest_pawn_normal():
    pawns = [
        Pawn((2, 4), 1),
        Pawn((3, 2), 1),
        Pawn((8, 8), 1)
    ]
    distance, closest_pawn = find_closest_pawn((1, 1), pawns)
    assert distance == 3
    assert closest_pawn == pawns[1]


def test_find_closest_pawn_zero_distance():
    pawns = [
        Pawn((2, 4), 2),
        Pawn((3, 2), 2),
        Pawn((8, 8), 2)
    ]
    distance, closest_pawn = find_closest_pawn((3, 2), pawns)
    assert distance == 0
    assert closest_pawn == pawns[1]


def test_find_closest_pawn_two_equally_distant():
    pawns = [
        Pawn((2, 3), 2),
        Pawn((3, 2), 2),
        Pawn((8, 8), 2)
    ]
    distance, closest_pawn = find_closest_pawn((1, 1), pawns)
    assert distance == 3
    assert closest_pawn in [pawns[0], pawns[1]]


def test_find_lowest_value_normal():
    items = ["a", "b", "c", "d"]
    values = [5, 24, 1, 3]
    lowest_item, value = find_lowest_value(items, values)
    assert lowest_item == "c"
    assert value == 1


def test_find_lowest_value_equal():
    items = ["a", "b", "c", "d"]
    values = [5, 1, 1, 3]
    lowest_item, value = find_lowest_value(items, values)
    assert lowest_item in ["b", "c"]
    assert value == 1


def test_order_pawns_by_distance_white():
    pawns = [
        Pawn((15, 4), 1),
        Pawn((14, 2), 1),
        Pawn((8, 8), 1)
    ]
    sorted_pawns = order_pawns_by_distance(pawns, 1)
    assert sorted_pawns[0].position() == pawns[1].position()
    assert sorted_pawns[1].position() == pawns[0].position()
    assert sorted_pawns[2].position() == pawns[2].position()


def test_order_pawns_by_distance_black():
    pawns = [
        Pawn((4, 15), 2),
        Pawn((2, 14), 2),
        Pawn((8, 8), 2)
    ]
    sorted_pawns = order_pawns_by_distance(pawns, 2)
    assert sorted_pawns[0].position() == pawns[1].position()
    assert sorted_pawns[1].position() == pawns[0].position()
    assert sorted_pawns[2].position() == pawns[2].position()


def test_order_pawns_by_distance_zero_distance():
    pawns = [
        Pawn((14, 2), 2),
        Pawn((16, 1), 2),
        Pawn((8, 8), 2)
    ]
    sorted_pawns = order_pawns_by_distance(pawns, 1)
    assert sorted_pawns[0].position() == pawns[1].position()
    assert sorted_pawns[1].position() == pawns[0].position()
    assert sorted_pawns[2].position() == pawns[2].position()


def test_order_pawns_by_distance_two_equally_distant():
    pawns = [
        Pawn((14, 2), 1),
        Pawn((15, 3), 1),
        Pawn((8, 8), 1)
    ]
    sorted_pawns = order_pawns_by_distance(pawns, 1)
    assert (
            sorted_pawns[0].position() == pawns[0].position()
            and sorted_pawns[1].position() == pawns[1].position()
           ) or (
            sorted_pawns[0].position() == pawns[0].position()
            and sorted_pawns[1].position() == pawns[1].position()
           )
    assert sorted_pawns[2].position() == pawns[2].position()


def test_find_all_moves_starting():
    board = Board()
    jumps = find_all_moves(board, 1)
    assert len(jumps) == 40


def test_find_all_moves_almost_winning_position():
    board = Board()
    pawns = board.pawns()
    camp = black_camp()
    for i in range(0, 19):
        pawns[i].set_position(camp[i])
    for i in range(19, 30):
        pawns[i].set_position((10, i-18))
    for i in range(30, 38):
        pawns[i].set_position((9, i-29))
    camp = white_camp()
    for i in range(19, 38):
        pawns[i].set_position(camp[i-20])
    pawns[18].set_position((1, 1))
    jumps = find_all_moves(board, 1)
    assert len(jumps) == 12
    jumps = find_all_moves(board, 2)
    assert len(jumps) == 0


def test_quality_after_move_white():
    board = Board()
    pawns = board.pawns()
    pawns[18].set_position((12, 4))
    quality = position_quality(board, 1)
    assert quality_after_move(board, 1, [(15, 1), (11, 5)]) == quality - 8


def test_quality_after_move_black():
    board = Board()
    pawns = board.pawns()
    pawns[37].set_position((4, 12))
    quality = position_quality(board, 2)
    assert quality_after_move(board, 2, [(1, 15), (5, 11)]) == quality - 8


def test_find_best_move_white():
    board = Board()
    pawns = board.pawns()
    pawns[18].set_position((12, 4))
    assert find_best_move(board, 1) == [(15, 1), (11, 5)]


def test_find_best_move_black():
    board = Board()
    pawns = board.pawns()
    pawns[37].set_position((4, 12))
    assert find_best_move(board, 2) == [(1, 15), (5, 11)]


def test_generate_move_towards():
    board = Board()
    pawns = board.pawns()
    pawns[37].set_position((4, 12))
    move = generate_move_towards(board, board.field_content((1, 15)), (16, 1))
    assert move == [(1, 15), (5, 11)]


def test_find_empty_fields_starting_position():
    board = Board()
    fields = find_empty_fields(board)
    assert len(fields) == 218
    assert (13, 3) not in fields
    assert (12, 4) in fields


def test_find_empty_fields_one_move():
    board = Board()
    board.move_pawn((13, 3), (12, 4))
    fields = find_empty_fields(board)
    assert len(fields) == 218
    assert (13, 3) in fields
    assert (12, 4) not in fields


def test_is_highway_true():
    goal = (1, 1)
    answer, value = is_highway((10, 10), (8, 5), goal)
    assert answer is True
    assert value == 7
    answer, value = is_highway((8, 5), (1, 5), goal)
    assert answer is True
    assert value == 7
    answer, value = is_highway((1, 1), (10, 10), (8, 8))
    assert answer is True
    assert value == 10


def test_is_highway_false():
    goal = (1, 1)
    answer, value = is_highway((8, 5), (16, 16), goal)
    assert answer is False
    assert value == -19
    answer, value = is_highway((7, 5), (1, 5), goal)
    assert answer is False
    assert value == 6
    answer, value = is_highway((10, 10), (10, 10), (8, 8))
    assert answer is False
    assert value == 0


def test_find_highways():
    board = Board()
    pawns = board.pawns()
    pawns[18].set_position((12, 4))
    pawns[1].set_position((10, 6))
    highways_white = find_highways(board, find_empty_fields(board), (1, 16))
    assert set(highways_white) == {(15, 1), (13, 3), (11, 1)}
    highways_black = find_highways(board, find_empty_fields(board), (16, 1))
    assert set(highways_black) == {(9, 7), (11, 5)}


def test_find_highways_none_available():
    board = Board()
    highways_white = find_highways(board, find_empty_fields(board), (1, 16))
    assert highways_white == []
    highways_black = find_highways(board, find_empty_fields(board), (16, 1))
    assert highways_black == []


def test_verify_highway_move_success():
    board = Board()
    pawns = board.pawns()
    pawns[18].set_position((12, 4))
    pawns[1].set_position((10, 6))
    result = verify_highway_move(board, (14, 4), (13, 3), (1, 16))
    assert result is True


def test_verify_highway_move_failure():
    board = Board()
    pawns = board.pawns()
    pawns[18].set_position((12, 4))
    pawns[1].set_position((10, 6))
    result = verify_highway_move(board, (12, 4), (13, 3), (1, 16))
    assert result is False


def test_find_move_to_highway():
    board = Board()
    pawns = board.pawns()
    pawns[18].set_position((12, 4))
    pawns[1].set_position((10, 6))
    location, destination = find_move_to_highway(board, 1)
    assert (
            destination == (11, 1) and location in
            [(12, 1), (13, 1), (15, 3), (15, 5)]
            ) or (
            destination == (15, 1) and location in [
                (13, 1), (15, 3), (15, 5), (16, 1),
                (14, 1), (15, 2), (14, 2), (16, 2)])


def test_find_move_to_highway_none_available():
    board = Board()
    move = find_move_to_highway(board, 1)
    assert move is None
    move = find_move_to_highway(board, 2)
    assert move is None


def test_pawns_to_pair_up_starting_position():
    board = Board()
    pawn1, pawn2 = pawns_to_pair_up(board, 1)
    assert pawn1.position() == (16, 1)
    assert pawn2.position() in [(15, 1), (16, 1)]


def test_pawns_to_pair_up_normal():
    board = Board()
    pawns = board.pawns()
    for i in range(19, 30):
        pawns[i].set_position((10, i-18))
    for i in range(30, 38):
        pawns[i].set_position((9, i-29))
    pawn1, pawn2 = pawns_to_pair_up(board, 2)
    assert pawn1.position() == (10, 11)
    assert pawn2.position() == (10, 10)


def test_pair_up_valid_success():
    board = Board()
    pawns = board.pawns()
    for i in range(19, 30):
        pawns[i].set_position((10, i-18))
    for i in range(30, 38):
        pawns[i].set_position((9, i-29))
    pawns[29].set_position((5, 12))
    pawns[30].set_position((7, 14))
    assert pair_up_valid(board, 2, 5, 12, 7, 14) is True


def test_pair_up_valid_too_far():
    board = Board()
    pawns = board.pawns()
    for i in range(19, 30):
        pawns[i].set_position((10, i-18))
    for i in range(30, 38):
        pawns[i].set_position((9, i-29))
    pawns[29].set_position((5, 12))
    assert pair_up_valid(board, 2, 5, 12, 9, 8) is False


def test_pair_up_valid_already_paired():
    board = Board()
    pawns = board.pawns()
    for i in range(19, 30):
        pawns[i].set_position((10, i-18))
    for i in range(30, 38):
        pawns[i].set_position((9, i-29))
    pawns[29].set_position((5, 12))
    pawns[30].set_position((6, 11))
    assert pair_up_valid(board, 2, 5, 12, 6, 11) is False


def test_pair_up_lone_move_towards():
    board = Board()
    pawns = board.pawns()
    for i in range(19, 30):
        pawns[i].set_position((10, i-18))
    for i in range(30, 38):
        pawns[i].set_position((9, i-29))
    pawns[29].set_position((4, 13))
    pawns[30].set_position((6, 11))
    location, destination = pair_up_lone(board, 2)
    assert location == (4, 13)
    assert destination == (5, 12)


def test_pair_up_lone_finish():
    board = Board()
    pawns = board.pawns()
    for i in range(19, 30):
        pawns[i].set_position((10, i-18))
    for i in range(30, 38):
        pawns[i].set_position((9, i-29))
    pawns[29].set_position((5, 11))
    pawns[30].set_position((6, 11))
    pawns[2].set_position((7, 10))
    location, destination = pair_up_lone(board, 2)
    assert location == (5, 11)
    assert destination == (5, 12)


def test_pair_up_lone_stuck():
    board = Board()
    pawns = board.pawns()
    for i in range(19, 30):
        pawns[i].set_position((10, i-18))
    for i in range(30, 38):
        pawns[i].set_position((9, i-29))
    pawns[29].set_position((6, 12))
    pawns[1].set_position((5, 12))
    pawns[30].set_position((6, 11))
    pawns[2].set_position((7, 11))
    move = pair_up_lone(board, 2)
    assert move is None


def test_pair_up_lone_edge():
    board = Board()
    pawns = board.pawns()
    for i in range(19, 30):
        pawns[i].set_position((10, i-18))
    for i in range(30, 38):
        pawns[i].set_position((9, i-29))
    pawns[29].set_position((1, 7))
    pawns[2].set_position((2, 6))
    pawns[30].set_position((1, 6))
    move = pair_up_lone(board, 2)
    assert move is None


def test_generate_move_normal():
    board = Board()
    pawns = board.pawns()
    pawns[18].set_position((12, 4))
    pawns[1].set_position((10, 6))
    location, destination = generate_move(board, 1)
    assert location in [(13, 1), (15, 3)]
    assert destination == (9, 7)


def test_generate_move_highway():
    board = Board()
    pawns = board.pawns()
    for i in range(19, 28):
        pawns[i].set_position((i-12, 16))
    for i in range(28, 38):
        pawns[i].set_position((i-21, 15))
    camp = black_camp()
    for i in range(0, 19):
        pawns[i].set_position(camp[i])
    pawns[18].set_position((16, 1))
    pawns[17].set_position((14, 3))
    pawns[16].set_position((12, 5))
    pawns[15].set_position((10, 7))
    location, destination = generate_move(board, 1)
    assert location == (16, 1)
    assert destination == (15, 2)


def test_generate_move_pair_up():
    board = Board()
    pawns = board.pawns()
    for i in range(19, 28):
        pawns[i].set_position((i-12, 16))
    for i in range(28, 38):
        pawns[i].set_position((i-21, 15))
    camp = black_camp()
    for i in range(0, 19):
        pawns[i].set_position(camp[i])
    pawns[17].set_position((13, 3))
    pawns[18].set_position((11, 5))
    location, destination = generate_move(board, 1)
    assert location == (13, 3)
    assert destination == (12, 4)


def test_generate_move_no_moves():
    board = Board()
    pawns = board.pawns()
    star_positions = [
        (9, 6), (8, 6), (11, 6), (12, 6), (10, 7), (10, 8), (10, 5), (10, 4),
        (11, 7), (12, 8), (9, 5), (8, 4), (11, 5), (12, 4), (9, 7), (8, 8)
    ]
    for i in range(19, 35):
        pawns[i].set_position(star_positions[i-19])
    pawns[35].set_position((3, 13))
    pawns[36].set_position((1, 1))
    pawns[37].set_position((2, 2))
    camp = black_camp()
    for i in range(0, 19):
        pawns[i].set_position(camp[i])
    pawns[18].set_position((10, 6))
    move = generate_move(board, 1)
    assert move is None

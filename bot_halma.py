from class_halma import Board, Pawn, white_camp, black_camp, \
                        validate_position, generate_possible_moves
from copy import deepcopy, copy
from typing import Optional, Any
from random import randint


def calculate_differences(field1: tuple[int, int], field2: tuple[int, int]) \
        -> tuple[int, int]:
    """
    Returns the difference in x and y between the specified fields.
    """
    x1, y1 = field1
    x2, y2 = field2
    dist_x = abs(x1 - x2)
    dist_y = abs(y1 - y2)
    return (dist_x, dist_y)


def distance_pythagorean(field1: tuple[int, int], field2: tuple[int, int]) \
        -> float:
    """
    Calculates the distance in a straight line between the specified fields.
    """
    dist_x, dist_y = calculate_differences(field1, field2)
    return (dist_x**2 + dist_y**2)**0.5


def distance_city_block(field1: tuple[int, int], field2: tuple[int, int]) \
        -> int:
    """
    Calculates the city block distance between the specified fields.
    """
    dist_x, dist_y = calculate_differences(field1, field2)
    return dist_x + dist_y


def player_pawns(board: Board, player: int) -> list[Pawn]:
    """
    Returns a list of specified player's pawns.
    """
    result = []
    for pawn in board.pawns():
        if pawn.owner() == player:
            result.append(pawn)
    return result


def find_lowest_value(items: list[Any], values: list[int]) -> tuple[Any, int]:
    """
    Takes a list of items and a list of values assigned to them.
    Returns the lowest valued item together with its value.
    If the lowest value is in a few places the first item is returned.
    """
    item_to_return = items[0]
    lowest_value = values[0]
    for item, value in zip(items, values):
        if value < lowest_value:
            lowest_value = value
            item_to_return = item
    return (item_to_return, lowest_value)


def find_closest_pawn(field: tuple[int, int], pawns: list[Pawn]) \
        -> tuple[int, Pawn]:
    """
    Finds a player's pawn among the given ones
    that is the closest to the specified field.
    Returns a tuple with the distance and the pawn.
    """
    distances = [distance_pythagorean(pawn.position(), field)
                 for pawn in pawns]
    pawn, _ = find_lowest_value(pawns, distances)
    distance = distance_city_block(pawn.position(), field)
    return (distance, pawn)


def position_quality(board: Board, player: int) -> int:
    """
    Checks how many single moves are needed to win the game.
    """
    result = 0
    my_pawns = player_pawns(board, player)
    opposing_corner = (1, 16) if player == 1 else (16, 1)
    for pawn in my_pawns:
        result += distance_city_block(pawn.position(), opposing_corner)
    if result > 100:
        return result
    # this method was good for making moves in the right direction
    # but won't work for fitting all pawns in the camp
    # for endgame positions:
    result = 0
    camp = black_camp() if player == 1 else white_camp()
    for field in camp:
        distance, closest_pawn = find_closest_pawn(field, my_pawns)
        my_pawns.remove(closest_pawn)
        result += distance
    return result


def order_pawns_by_distance(pawns: list[Pawn], player: int) -> list[Pawn]:
    """
    Creates a list that sorts pawns' distance to the corner of the player's
    camp in ascending order and returns it.
    """
    pawns_left = copy(pawns)
    ordered_pawns = []
    own_corner = (16, 1) if player == 1 else (1, 16)
    while len(pawns_left) > 0:
        _, closest_pawn = find_closest_pawn(own_corner, pawns_left)
        ordered_pawns.append(closest_pawn)
        pawns_left.remove(closest_pawn)
    return ordered_pawns


def find_all_moves(board: Board, player: int) -> list[list[tuple[int, int]]]:
    """
    Returns all available moves for the specified player on a specified board.
    Returns an empty list if there are no moves.
    """
    my_pawns = player_pawns(board, player)
    if randint(0, 1) == 1:
        # gives the least advanced pawns a higher chance to be moved
        my_pawns = order_pawns_by_distance(my_pawns, player)
    all_moves = []
    for pawn in my_pawns:
        pawn_moves = board.check_possible_moves(pawn.position())
        pawn_moves = board.remove_moves_out_of_camp(
                     player, pawn.position(), pawn_moves)
        for move in pawn_moves:
            all_moves.append([pawn.position(), move])
    return all_moves


def quality_after_move(board: Board, player: int,
                       move: list[tuple[int, int]]) -> int:
    """
    Performs the specified move on a copy of the passed board
    and returns the quality of the new position.
    """
    position, destination = move
    board_after = deepcopy(board)
    board_after.move_pawn(position, destination)
    quality = position_quality(board_after, player)
    return quality


def find_best_move(board: Board, player: int) -> list[tuple[int, int]]:
    """
    The brute-force part of analyzing possible moves.
    Finds out how good is each move and returns the best one.
    Needs a board where moves are available.
    """
    moves = find_all_moves(board, player)
    move_qualities = [quality_after_move(board, player, move)
                      for move in moves]
    best_move, _ = find_lowest_value(moves, move_qualities)
    return best_move


def generate_move_towards(board: Board, pawn: Pawn, field: tuple[int, int]) \
        -> Optional[list[tuple[int, int]]]:
    """
    Takes a pawn and finds the best available move to get to specified field.
    Returns None if the pawn doesn't get closer to the field.
    """
    original_distance = distance_city_block(pawn.position(), field)
    moves = board.check_possible_moves(pawn.position())
    new_distances = [distance_city_block(destination, field)
                     for destination in moves]
    best_destination, best_distance = find_lowest_value(moves, new_distances)
    if original_distance - best_distance > 0:
        return [pawn.position(), best_destination]
    return None


def find_empty_fields(board: Board) -> list[tuple[int, int]]:
    """
    Finds all empty fields on the specified board and returns them in a list.
    """
    fields = []
    for y in range(1, 17):
        for x in range(1, 17):
            fields.append((x, y))
    empty_fields = []
    for field in fields:
        if board.field_content(field) is None:
            empty_fields.append(field)
    return empty_fields


def is_highway(location: tuple[int, int], destination: tuple[int, int],
               goal: tuple[int, int]) -> tuple[bool, int]:
    """
    Checks if a highway lets a pawn get closer to the desired field
    by more than 6 city block tiles. Returns the answer and the amount.
    """
    distance_old = distance_city_block(location, goal)
    distance_new = distance_city_block(destination, goal)
    quality = distance_old - distance_new
    if quality > 6:
        return (True, quality)
    return (False, quality)


def find_highways(board: Board, empty_fields: list[tuple[int, int]],
                  opposing_corner: tuple[int, int]) -> list[tuple[int, int]]:
    """
    Scans the empty fields on the board and finds highways that lead to
    the specified corner. Returns them in a list - sorted descending
    by their usefulness so that they are considered first.
    Returns an empty list if they don't exist.
    """
    highways = []
    values = []
    for field in empty_fields:
        moves = board.check_possible_moves(field)
        for destination in moves:
            useful, quality = is_highway(field, destination, opposing_corner)
            if useful and field in highways:
                for highway, best_quality in zip(highways, values):
                    if field == highway and quality > best_quality:
                        best_quality = quality
            if useful and field not in highways:
                highways.append(field)
                values.append(quality)
    sorted_highways_with_values = sorted(zip(highways, values), reverse=True,
                                         key=lambda values: values[1])
    return [highway for highway, _ in sorted_highways_with_values]


def verify_highway_move(board: Board, pawn_field: tuple[int, int],
                        highway: tuple[int, int],
                        opposing_corner: tuple[int, int]) -> bool:
    """
    A pawn that is about to be moved to the beginning of the highway
    may turn out to be a part of it and end up breaking it and wasting a move.
    This fuction verifies if after the move a good move is still available.
    """
    board_after = deepcopy(board)
    board_after.move_pawn(pawn_field, highway)
    moves = board_after.check_possible_moves(highway)
    for destination in moves:
        valid, _ = is_highway(pawn_field, destination, opposing_corner)
        if valid:
            return True
    return False


def find_move_to_highway(board: Board, player: int) \
        -> Optional[list[tuple[int, int]]]:
    """
    Sometimes there are fields that can transport a pawn over a large distance.
    They'll be referred to as highways here. This function detects such spots
    and if a pawn is around it, the returned move causes the pawn to go
    towards that field.
    The function returns None if there are no such fields to easily get to.
    """
    empty_fields = find_empty_fields(board)
    opposing_corner = (1, 16) if player == 1 else (16, 1)
    highways = find_highways(board, empty_fields, opposing_corner)
    for highway in highways:
        fields_around = [field for field in generate_possible_moves(highway, 1)
                         if validate_position(field)]
        for field in fields_around:
            pawn = board.field_content(field)
            if pawn is not None and pawn.owner() == player \
               and verify_highway_move(board, pawn.position(), highway,
                                       opposing_corner):
                return [field, highway]
    return None


def pawns_to_pair_up(board: Board, player: int) -> tuple[Pawn, Pawn]:
    """
    Finds two pawns that are the least advanced
    (the closest to the corner of player's own camp).
    Returns them in a tuple, the closest one is first.
    """
    own_corner = (16, 1) if player == 1 else (1, 16)
    remaining_pawns = player_pawns(board, player)
    _, closest_pawn = find_closest_pawn(own_corner, remaining_pawns)
    remaining_pawns.remove(closest_pawn)
    _, next_closest_pawn = find_closest_pawn(own_corner, remaining_pawns)
    return (closest_pawn, next_closest_pawn)


def pair_up_valid(board: Board, player: int, x1: int, y1: int,
                  x2: int, y2: int) -> bool:
    """
    (Pass the coordinates for the less advanced pawn into x1 and y1).
    Verifies if the two least advanced pawns should be paired up:
    - if the second pawn is too far away, the moves spent on pairing them up
      might not pay off before they reach the opposing camp
    - if they're already paired up, the operation is pointless
    """
    own_corner = (16, 1) if player == 1 else (1, 16)
    distance = distance_city_block((x2, y2), own_corner)
    if distance > 14:
        return False
    in_front_of_first = board.field_content((x1 - 1, y1 + 1)) if player == 1 \
        else board.field_content((x1 + 1, y1 - 1))
    if in_front_of_first is not None and (
        (player == 1 and in_front_of_first.owner() == 1)
        or (player == 2 and in_front_of_first.owner() == 2)
    ):
        return False
    return True


def pair_up_lone(board: Board, player: int) -> Optional[list[tuple[int, int]]]:
    """
    Attempts to pair up the least advanced pawns on the board
    (one is diagonally in front of the other) so that they can move faster.
    Returns None if the operation is not worth it.
    """
    closer_pawn, further_pawn = pawns_to_pair_up(board, player)
    x1, y1 = closer_pawn.position()
    x2, y2 = further_pawn.position()
    if not pair_up_valid(board, player, x1, y1, x2, y2):
        return None
    distance = distance_city_block((x1, y1), (x2, y2))
    if distance > 2:
        # pawns are far away, get the first one generally closer to the second
        return generate_move_towards(board, closer_pawn, (x2, y2))
    else:
        # move the first pawn behind the second one
        pawn_to_move = closer_pawn
        field = (x2 + 1, y2 - 1) if player == 1 else (x2 - 1, y2 + 1)
        if (
            not validate_position(field)
            # for edge fields the field behind them does not exist
            or board.field_content(field) is not None
        ):
            # move the second pawn ahead of the first one
            pawn_to_move = further_pawn
            field = (x1 - 1, y1 + 1) if player == 1 else (x1 + 1, y1 - 1)
            if board.field_content(field) is not None:
                return None  # both spaces are taken, give up pairing for now
        return generate_move_towards(board, pawn_to_move, field)


def generate_move(board: Board, player: int) \
        -> Optional[list[tuple[int, int]]]:
    """
    Takes a board, analyses it from specified player's perspective
    and returns the best move that was found.
    Returns None if there are no moves.
    """
    if find_all_moves(board, player) == []:
        return None
    best_move = find_best_move(board, player)
    before = position_quality(board, player)
    after = quality_after_move(board, player, best_move)
    difference = before - after
    if difference < 6:
        # no great moves with the first method, check additional ones
        considerable_move = find_move_to_highway(board, player)
        if considerable_move:
            best_move = considerable_move
        else:
            considerable_move = pair_up_lone(board, player)
        if considerable_move:
            best_move = considerable_move
    return best_move

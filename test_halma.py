from halma import process_command


def test_process_command_quit():
    assert process_command("quit") == [(-1, -1)]


def test_process_command_help():
    assert process_command("help") == [(0, -1)]


def test_process_command_check_moves():
    assert process_command("a1") == [(1, 1)]
    assert process_command("h10") == [(8, 10)]
    assert process_command("p16") == [(16, 16)]

    assert process_command("a0") == [(-2, -2)]
    assert process_command("h17") == [(-2, -2)]
    assert process_command("z4") == [(-2, -2)]


def test_process_command_move():
    assert process_command("a1 b8") == [(1, 1), (2, 8)]
    assert process_command("c4 p16") == [(3, 4), (16, 16)]
    assert process_command("d11 a1") == [(4, 11), (1, 1)]

    assert process_command("a0 a1") == [(-2, -2)]
    assert process_command("a1 h17") == [(-2, -2)]
    assert process_command("z4 b4") == [(-2, -2)]


def test_process_command_invalid():
    assert process_command("") == [(-2, -2)]
    assert process_command("vehvfkvhek") == [(-2, -2)]
    assert process_command("72727") == [(-2, -2)]
    assert process_command("a0 a1 b4") == [(-2, -2)]
    assert process_command("e") == [(-2, -2)]
    assert process_command("zwdjchk fwehkj ejwk") == [(-2, -2)]

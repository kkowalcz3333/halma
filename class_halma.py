from typing import Optional


class InvalidCoordinatesException(Exception):
    def __init__(self):
        super().__init__("Pawn coordinates have to be between 1 and 16")


class InvalidOwnerException(Exception):
    def __init__(self):
        super().__init__("Owner number has to be either 1 or 2")


class IllegalMoveException(Exception):
    def __init__(self):
        super().__init__("This move cannot be executed")


class PawnNotFoundException(Exception):
    def __init__(self):
        super().__init__("This field does not contain a pawn")


class NegativeDistanceException(Exception):
    def __init__(self):
        super().__init__("Distance has to be positive")


class Pawn():
    """
    Class pawn. Has attributes:
    :param position: Position on the board in x, y.
                     The values have to be between 1 and 16.
    :type position: (int, int)

    :param owner: Owner (color) of the pawn. Has to be 1 or 2.
                  1 means player 1 (white - 88 on the board).
                  2 means player 2 (black - [] on the board).
    :type owner: int
    """
    def __init__(self, position, owner):
        if not validate_position((position)):
            raise InvalidCoordinatesException
        if not validate_owner(owner):
            raise InvalidOwnerException
        self._position = position
        self._owner = owner

    def position(self):
        return self._position

    def owner(self):
        return self._owner

    def set_position(self, new_position: tuple[int, int]):
        if not validate_position(new_position):
            raise InvalidCoordinatesException
        self._position = new_position


class Board():
    """
    Class board. Has attributes:
    :param pawns: A list of all 38 pawns on the board.
    :type pawns: list[Pawn]
    """
    def __init__(self):
        self._pawns = generate_pawns()

    def pawns(self):
        return self._pawns

    def field_content(self, position: tuple[int, int]) -> Optional[Pawn]:
        """
        Checks what is on a field with given coordinates.
        Returns the pawn on that field or None if it's empty.
        """
        if not validate_position(position):
            raise InvalidCoordinatesException
        for pawn in self.pawns():
            if (pawn.position() == position):
                return pawn
        return None

    def check_possible_moves(self, position: tuple[int, int]) \
            -> list[tuple[int, int]]:
        """
        Lists all possible moves and jumps from the specified position.
        The original position is never included,
        the rule about not leaving the camp is not considered.
        This method is only for moves but it also calls the jump function.
        """
        if not validate_position(position):
            raise InvalidCoordinatesException
        found_positions = []
        possible_moves = generate_possible_moves(position, 1)
        for new_position in possible_moves:
            if (
                validate_position(new_position)
                and not self.field_content(new_position)
            ):
                found_positions.append(new_position)
        self.check_possible_jumps(position, found_positions)
        if position in found_positions:
            found_positions.remove(position)
        return found_positions

    def check_possible_jumps(self, position: tuple[int, int],
                             found_positions: list[tuple[int, int]]) -> None:
        """
        Lists all possible coordinates to jump to from the given position.
        Then recursively analyzes all possible jumps from new positions.
        Results are added to found_positions.
        """
        possible_moves = generate_possible_moves(position, 2)
        for new_position in possible_moves:
            if (
                validate_position(new_position)
                and not self.field_content(new_position)
                and self.field_content(
                    ((position[0] + new_position[0])/2,
                     (position[1] + new_position[1])/2)
                )
                and new_position not in found_positions
            ):
                found_positions.append(new_position)
                self.check_possible_jumps(new_position, found_positions)

    def remove_moves_out_of_camp(self, player: int, position: tuple[int, int],
                                 moves: list[tuple[int, int]]) \
            -> list[tuple[int, int]]:
        """
        Takes a list of possible moves from the specified position
        and the player's perspective to consider.
        Returns a list of moves that don't cause leaving the opposing camp.
        """
        camp = black_camp() if player == 1 else white_camp()
        if position not in camp:
            return moves
        result = []
        for move in moves:
            if move in camp:
                result.append(move)
        return result

    def move_pawn(self, old_position: tuple[int, int],
                  new_position: tuple[int, int]) -> None:
        """
        Moves a pawn between two specified positions (if possible).
        """
        pawn = self.field_content(old_position)
        if not pawn:
            raise PawnNotFoundException
        if new_position not in self.check_possible_moves(old_position):
            raise IllegalMoveException
        pawn.set_position(new_position)

    def check_winning_position(self) -> Optional[int]:
        """
        Checks if either player is in a winning position.
        Returns the number of the winning player or None if none won.
        """
        white_winning = True
        black_winning = True
        for pawn in self.pawns():
            if white_winning and pawn.owner() == 1 \
                    and pawn.position() not in black_camp():
                white_winning = False
            elif black_winning and pawn.owner() == 2 \
                    and pawn.position() not in white_camp():
                black_winning = False
        if white_winning:
            return 1
        if black_winning:
            return 2
        return None

    def check_if_stuck(self) -> Optional[int]:
        """
        Checks if any player is stuck (some pawns are in the old base
        without being able to move, whereas the rest is in the opposing one).
        Returns the number of the stuck player or None if none are.
        """
        if self.check_winning_position():
            return None
        whites_stuck = True
        blacks_stuck = True
        for pawn in self.pawns():
            moves = self.check_possible_moves(pawn.position())
            if whites_stuck and pawn.owner() == 1 and not (
                pawn.position() in black_camp()
                or (pawn.position() in white_camp() and moves == [])
            ):
                whites_stuck = False
            elif blacks_stuck and pawn.owner() == 2 and not (
                pawn.position() in white_camp()
                or (pawn.position() in black_camp() and moves == [])
            ):
                blacks_stuck = False
        if whites_stuck:
            return 1
        if blacks_stuck:
            return 2
        return None


def validate_position(coordinates: tuple[int, int]) -> bool:
    """
    Takes a tuple of two coordinates and checks if they fit on the board.
    """
    x, y = coordinates
    if (x < 1 or x > 16 or y < 1 or y > 16):
        return False
    return True


def validate_owner(owner: int) -> bool:
    """
    Checks if the specified owner number is valid (equals 1 or 2).
    """
    if owner not in [1, 2]:
        return False
    return True


def generate_pawns() -> list[Pawn]:
    """
    Creates pawns in their starting positions and returns them in a list.
    """
    pawns = []
    for position in white_camp():
        pawns.append(Pawn(position, 1))
    for position in black_camp():
        pawns.append(Pawn(position, 2))
    return pawns


def white_camp() -> list[tuple[int, int]]:
    """
    Returns a list of positions belonging to the white camp.
    """
    return [
        (16, 1), (15, 1), (16, 2), (14, 1), (16, 3), (15, 2), (13, 1), (16, 4),
        (14, 2), (15, 3), (16, 5), (12, 1), (13, 2), (15, 4), (14, 3), (12, 2),
        (15, 5), (14, 4), (13, 3),
    ]


def black_camp() -> list[tuple[int, int]]:
    """
    Returns a list of positions belonging to the black camp.
    """
    return [
        (1, 16), (1, 15), (2, 16), (1, 14), (3, 16), (2, 15), (1, 13), (4, 16),
        (2, 14), (3, 15), (5, 16), (1, 12), (2, 13), (4, 15), (3, 14), (2, 12),
        (5, 15), (4, 14), (3, 13),
    ]


def generate_possible_moves(position: tuple[int, int], distance) \
        -> list[tuple[int, int]]:
    """
    Takes the current position and returns a list of positions after
    vertical, horizontal and diagonal moves by specified amount of tiles.
    Distance has to be positive.
    """
    if distance < 1:
        raise NegativeDistanceException
    x, y = position
    possible_moves = [
        (x+distance, y+distance),
        (x-distance, y-distance),
        (x+distance, y-distance),
        (x-distance, y+distance),
        (x,          y+distance),
        (x,          y-distance),
        (x+distance, y),
        (x-distance, y),
    ]
    return possible_moves

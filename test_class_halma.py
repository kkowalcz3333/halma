from class_halma import InvalidCoordinatesException, \
                        InvalidOwnerException, \
                        IllegalMoveException, \
                        PawnNotFoundException, \
                        NegativeDistanceException, \
                        validate_position, validate_owner, \
                        generate_pawns, generate_possible_moves, \
                        white_camp, black_camp, \
                        Pawn, Board
import pytest


def test_pawn_init_normal():
    pawn = Pawn((8, 2), 1)
    assert pawn.position() == (8, 2)
    assert pawn.owner() == 1


def test_pawn_init_edge_values():
    pawn = Pawn((1, 16), 2)
    assert pawn.position() == (1, 16)
    assert pawn.owner() == 2


def test_pawn_set_position_normal():
    pawn = Pawn((1, 16), 2)
    pawn.set_position((12, 4))
    assert pawn.position() == (12, 4)
    pawn.set_position((3, 14))
    assert pawn.position() == (3, 14)


def test_pawn_set_position_wrong():
    pawn = Pawn((1, 16), 2)
    with pytest.raises(InvalidCoordinatesException):
        pawn.set_position((0, 4))
    with pytest.raises(InvalidCoordinatesException):
        pawn.set_position((17, 4))
    with pytest.raises(InvalidCoordinatesException):
        pawn.set_position((12, 44))
    with pytest.raises(InvalidCoordinatesException):
        pawn.set_position((-20, 41))


def test_validate_position_success():
    positions = [(12, 6), (1, 1), (16, 16), (1, 16), (16, 1), (5, 11)]
    for position in positions:
        assert validate_position(position) is True


def test_validate_position_failure():
    positions = [(0, 5), (8, 0), (17, 1), (11, 17), (-40, 18), (564, -81)]
    for position in positions:
        assert validate_position(position) is False


def test_pawn_init_wrong_coordinates():
    with pytest.raises(InvalidCoordinatesException):
        Pawn((1, 0), 1)
    with pytest.raises(InvalidCoordinatesException):
        Pawn((1, 17), 1)
    with pytest.raises(InvalidCoordinatesException):
        Pawn((1, -20), 1)
    with pytest.raises(InvalidCoordinatesException):
        Pawn((0, 3), 1)
    with pytest.raises(InvalidCoordinatesException):
        Pawn((17, 2), 1)
    with pytest.raises(InvalidCoordinatesException):
        Pawn((-40, 1), 1)


def test_validate_owner_success():
    owners = [1, 2]
    for owner in owners:
        assert validate_owner(owner) is True


def test_validate_owner_failure():
    owners = [0, -1, -44, 65, 3, 8]
    for owner in owners:
        assert validate_owner(owner) is False


def test_pawn_init_wrong_owner():
    with pytest.raises(InvalidOwnerException):
        Pawn((6, 5), 0)
    with pytest.raises(InvalidOwnerException):
        Pawn((9, 11), 3)
    with pytest.raises(InvalidOwnerException):
        Pawn((1, 16), -84)


def test_generate_pawns():
    list = generate_pawns()
    assert len(list) == 38
    assert list[1].position() == (15, 1)
    assert list[1].owner() == 1


def test_board_init():
    board = Board()
    pawns = board.pawns()
    assert len(pawns) == 38
    assert pawns[28].position() == (3, 15)
    assert pawns[28].owner() == 2


def test_field_content_blank():
    board = Board()
    assert board.field_content((7, 7)) is None
    assert board.field_content((6, 8)) is None


def test_field_content_black():
    board = Board()
    assert board.field_content((1, 16)).owner() == 2
    assert board.field_content((2, 13)).owner() == 2


def test_field_content_white():
    board = Board()
    assert board.field_content((16, 1)).owner() == 1
    assert board.field_content((14, 3)).owner() == 1


def test_field_content_wrong():
    board = Board()
    with pytest.raises(InvalidCoordinatesException):
        board.field_content((1, 21))
    with pytest.raises(InvalidCoordinatesException):
        board.field_content((0, 2))


def test_generate_moves():
    correct_moves = {
        (10, 10),
        (0, 10),
        (10, 0),
        (-10, -10),
        (10, -10),
        (0, -10),
        (-10, 0),
        (-10, 10),
    }
    assert set(generate_possible_moves((0, 0), 10)) == correct_moves


def test_generate_moves_non_positive_distance():
    with pytest.raises(NegativeDistanceException):
        generate_possible_moves((0, 0), -10)
    with pytest.raises(NegativeDistanceException):
        generate_possible_moves((0, 0), 0)


def test_check_possible_moves_clear_in_corner():
    possible_moves = set([(1, 2), (2, 1), (2, 2)])
    board = Board()
    found_moves = board.check_possible_moves((1, 1))
    assert set(found_moves) == possible_moves


def test_check_possible_moves_no_moves():
    board = Board()
    found_moves = board.check_possible_moves((1, 16))
    assert found_moves == []


def test_check_possible_moves_normal():
    possible_moves = set([(5, 14), (3, 12), (4, 13)])
    board = Board()
    found_moves = board.check_possible_moves((3, 14))
    assert set(found_moves) == possible_moves


def test_check_possible_moves_special():
    possible_moves = set([(12, 3), (15, 4), (13, 4),
                          (11, 4), (11, 2), (15, 6)])
    board = Board()
    board.move_pawn((13, 2), (13, 4))
    board.move_pawn((14, 4), (12, 4))
    board.move_pawn((13, 4), (11, 4))
    board.move_pawn((11, 4), (11, 3))
    board.move_pawn((15, 4), (14, 4))
    found_moves = board.check_possible_moves((13, 2))
    assert set(found_moves) == possible_moves


def test_check_possible_moves_wrong():
    board = Board()
    with pytest.raises(InvalidCoordinatesException):
        board.check_possible_moves((3, 17))
    with pytest.raises(InvalidCoordinatesException):
        board.check_possible_moves((0, 1))


def test_move_pawn_normal():
    board = Board()
    board.move_pawn((13, 2), (13, 4))
    assert board.field_content((13, 4)) is not None


def test_move_pawn_no_pawn():
    board = Board()
    with pytest.raises(PawnNotFoundException):
        board.move_pawn((1, 1), (2, 2))


def test_move_pawn_out_of_board():
    board = Board()
    with pytest.raises(IllegalMoveException):
        board.move_pawn((16, 1), (17, 1))


def test_move_pawn_field_taken():
    board = Board()
    with pytest.raises(IllegalMoveException):
        board.move_pawn((16, 1), (15, 1))


def test_move_pawn_too_far():
    board = Board()
    with pytest.raises(IllegalMoveException):
        board.move_pawn((12, 1), (10, 1))


def test_move_pawn_wrong_coordinates():
    board = Board()
    with pytest.raises(InvalidCoordinatesException):
        board.move_pawn((17, 1), (16, 1))


def test_check_winning_position_none():
    board = Board()
    assert board.check_winning_position() is None


def test_check_winning_position_white():
    board = Board()
    pawns = board.pawns()
    camp = black_camp()
    for i in range(0, 19):
        pawns[i].set_position(camp[i])
    for i in range(19, 30):
        pawns[i].set_position((10, i-18))
    for i in range(30, 38):
        pawns[i].set_position((9, i-29))
    assert board.check_winning_position() == 1


def test_check_winning_position_black():
    board = Board()
    pawns = board.pawns()
    camp = white_camp()
    for i in range(0, 11):
        pawns[i].set_position((10, i+1))
    for i in range(11, 19):
        pawns[i].set_position((9, i-10))
    for i in range(19, 38):
        pawns[i].set_position(camp[i-20])
    assert board.check_winning_position() == 2


def test_remove_moves_out_of_camp_outside():
    board = Board()
    moves = board.check_possible_moves((8, 8))
    cut_moves = board.remove_moves_out_of_camp(1, (8, 8), moves)
    assert moves == cut_moves


def test_remove_moves_out_of_camp_white():
    board = Board()
    board.pawns()[28].set_position((1, 1))
    moves = board.check_possible_moves((3, 14))
    cut_moves = board.remove_moves_out_of_camp(1, (3, 14), moves)
    assert cut_moves == [(3, 15)]


def test_remove_moves_out_of_camp_no_moves():
    board = Board()
    moves = board.check_possible_moves((3, 14))
    cut_moves = board.remove_moves_out_of_camp(1, (3, 14), moves)
    assert cut_moves == []


def test_remove_moves_out_of_camp_black():
    board = Board()
    board.pawns()[8].set_position((1, 1))
    moves = board.check_possible_moves((14, 3))
    cut_moves = board.remove_moves_out_of_camp(2, (14, 3), moves)
    assert cut_moves == [(14, 2)]


def test_check_if_stuck_not():
    board = Board()
    assert board.check_if_stuck() is None
    pawns = board.pawns()
    pawns[37].set_position((1, 1))
    pawns[0].set_position((2, 12))
    assert board.check_if_stuck() is None


def test_check_if_stuck_black():
    board = Board()
    pawns = board.pawns()
    for i in range(0, 11):
        pawns[i].set_position((10, i+1))
    for i in range(11, 19):
        pawns[i].set_position((9, i-10))
    for i in range(19, 38):
        pawns[i].set_position(white_camp()[i-20])
    for pawn, position in zip(pawns, black_camp()):
        pawn.set_position(position)
    pawns[0].set_position((1, 1))
    pawns[19].set_position((1, 16))
    assert board.check_if_stuck() == 2


def test_check_if_stuck_white():
    board = Board()
    pawns = board.pawns()
    for i in range(0, 11):
        pawns[i].set_position((10, i+1))
    for i in range(11, 19):
        pawns[i].set_position((9, i-10))
    for i in range(19, 38):
        pawns[i].set_position(white_camp()[i-20])
    for pawn, position in zip(pawns, black_camp()):
        pawn.set_position(position)
    pawns[37].set_position((1, 1))
    pawns[0].set_position((15, 1))
    assert board.check_if_stuck() == 1


def test_check_if_stuck_black_winning():
    board = Board()
    pawns = board.pawns()
    camp = white_camp()
    for i in range(0, 11):
        pawns[i].set_position((10, i+1))
    for i in range(11, 19):
        pawns[i].set_position((9, i-10))
    for i in range(19, 38):
        pawns[i].set_position(camp[i-20])
    assert board.check_if_stuck() is None

from class_halma import Board
import os
from platform import system


def clear() -> None:
    """
    Clears the screen.
    """
    if system() == "Windows":
        os.system("cls")
    else:
        os.system("clear")


def print_board(board: Board) -> None:
    """
    Clears the screen and prints a text representation of the specified board.
    """
    clear()
    for y in range(16, 0, -1):
        print("  +" + "----+"*16)
        row = " " if y < 10 else ""
        row += str(y) + "|"
        for x in range(1, 17):
            pawn = board.field_content((x, y))
            if not pawn:
                row += "    |"
            elif pawn.owner() == 1:
                row += " 88 |"
            else:
                row += " [] |"
        print(row)
    print("  +" + "----+"*16)
    print("     a    b    c    d    e    f    g    h "
          + "   i    j    k    l    m    n    o    p")
    print()


def help_sequence() -> None:
    """
    Clears the screen and prints instructions on how to play.
    """
    clear()
    print("GAME")
    print()
    print("The game takes place on a 16x16 board, with 2 players having 19 " +
          "pawns each. The objective is to be the first to move your pawns " +
          "from one corner (camp) of the board to the opposite one.")
    print()
    print("You can move your pawns in 8 directions by 1 square. You can " +
          "also jump over a single pawn, if there is space behind it (the " +
          "jumped pawn stays on the board). You can keep jumping in that " +
          "turn, but you can't move and jump in the same turn. Also you " +
          "need to finish jumping on another field than where you started.")
    print()
    print("Once a pawn finishes a turn inside the other camp, it cannot " +
          "finish future turns outside of it. Also make sure to remove " +
          "pawns from your own camp - if some get stuck there while the " +
          "rest is in the opposing camp, you lose.")
    print()
    print("CONTROLS")
    print()
    print("The board will be printed on the screen. White pawns are marked " +
          "as 88 and black as []. Once you decide on your move, enter the " +
          "coordinates of the pawn and the place where you want it moved. " +
          "Your input should look like this:")
    print("a11 b12")
    print()
    print("It will move the pawn on a11 to b12 (if the move is allowed). " +
          "If you want to perform a few jumps, enter the final destination." +
          "You can check the list of allowed destinations by entering the " +
          "coordinates of the pawn (or also empty field) that interests you:")
    print("a11")
    print()
    print("To leave the game, wait for your turn and enter:")
    print("quit")
    print()
    print("During the game you can return to this screen by entering:")
    print("help")
    print()


def print_menu_screen() -> None:
    """
    Clears the screen and prints the name of the game with options.
    """
    clear()
    print("  _    _       _                 ")
    print(" | |  | |     | |                ")
    print(" | |__| | __ _| |_ __ ___   __ _ ")
    print(" |  __  |/ _` | | '_ ` _ \\ / _` |")
    print(" | |  | | (_| | | | | | | | (_| |")
    print(" |_|  |_|\\__,_|_|_| |_| |_|\\__,_|")
    print()
    print("New game")
    print("1    Player vs Computer")
    print("2    Player vs Player")
    print("3    Computer vs Computer")
    print()
    print("Help")
    print("4    How to play")
    print()
    print("Leave")
    print("5    Quit")
    print()


def get_menu_input() -> str:
    """
    Asks for the main menu input and returns the choice.
    """
    print("Select the option...")
    choice = input()
    print()
    return choice.strip()


def print_menu_input_fail() -> None:
    """
    Informs the user that the input was invalid.
    """
    print("Invalid input. Enter a number between 1 and 5.")
    print()


def first_move_info(player1_is_human: bool) -> None:
    """
    Prints information about who got the white pieces.
    """
    if player1_is_human:
        print("You got the white 88 pieces and move first.")
    else:
        print("The computer got the white 88 pieces and moves first.")
    print()


def x_dictionary() -> dict[str, int]:
    """
    Mapping of board "x" values from letters to numbers.
    """
    return {
        "a": 1,
        "b": 2,
        "c": 3,
        "d": 4,
        "e": 5,
        "f": 6,
        "g": 7,
        "h": 8,
        "i": 9,
        "j": 10,
        "k": 11,
        "l": 12,
        "m": 13,
        "n": 14,
        "o": 15,
        "p": 16,
    }


def x_reversed_dictionary() -> dict[int, str]:
    """
    Mapping of board "x" values from numbers to letters.
    """
    return {
        1: "a",
        2: "b",
        3: "c",
        4: "d",
        5: "e",
        6: "f",
        7: "g",
        8: "h",
        9: "i",
        10: "j",
        11: "k",
        12: "l",
        13: "m",
        14: "n",
        15: "o",
        16: "p",
    }


def get_command(player: int) -> str:
    """
    Prints who is playing in this turn.
    Asks the player what they want to do in their turn.
    Returns the input in lowercase.
    """
    start = "White" if player == 1 else "Black"
    print(start + " to move. Choose your action...")
    choice = input()
    print()
    return choice.strip().lower()


def print_command_fail(mode: int) -> None:
    """
    Prints the information that the player's command is invalid.
    States the reason depending on passed mode number.
    """
    if mode == 0:
        print("Invalid command.")
    elif mode == 1:
        print("There is no pawn on that field.")
    elif mode == 2:
        print("This pawn is not yours.")
    elif mode == 3:
        print("This move is not allowed.")
    else:
        print("Your pawn has entered the opponent's camp and cannot leave it.")
    print("To find out more about the game you can enter:")
    print("help")
    print()


def print_moves(moves: list[tuple[int, int]]) -> None:
    """
    Prints the passed list of available moves from some position.
    """
    if len(moves) == 0:
        print("You can't move from this field")
        print()
        return
    line = "From this field you could move to: "
    for move in moves:
        line += (format_move_to_text(move) + ", ")
    print(line[:-2])
    print()


def format_move_to_text(move: tuple[int, int]) -> str:
    """
    Takes a position tuple and turns it into a readable string like "a1".
    """
    x, y = move
    result = x_reversed_dictionary()[x] + str(y)
    return result


def print_computer_turn() -> None:
    """
    Informs the player that the computer is generating the move.
    """
    print("The computer is considering its options...")
    print()


def print_computer_decision(move: list[tuple[int, int]]) -> None:
    """
    Prints info about computer's performed move.
    """
    pos1 = format_move_to_text(move[0])
    pos2 = format_move_to_text(move[1])
    print(f"The computer has moved from {pos1} to {pos2}.")
    print()


def checkpoint() -> None:
    """
    Asks the user to press enter, then clears the screen.
    """
    print("Press enter to continue...")
    input()
    clear()


def print_winner(player: int) -> None:
    """
    Prints info about who won the game.
    """
    color = "white" if player == 1 else "black"
    print(f"The player with {color} pieces won - congratulations!")
    print()


def print_stuck(stuck: int) -> None:
    """
    Prints info about which player got stuck.
    """
    color = "white" if stuck == 1 else "black"
    print(f"The player with {color} pieces got stuck and lost the game.")
    print()


def computer_checkpoint() -> bool:
    """
    Asks the user to press enter or quit, then clears the screen.
    False on output means doing nothing, True - quitting the game.
    """
    print("Press enter to continue... (you can leave by entering: quit)")
    choice = input()
    choice = choice.strip().lower()
    clear()
    if choice == "quit":
        return True
    else:
        return False


def no_moves() -> None:
    """
    It is possible for the user to make the computer run out of moves
    without triggering the "stuck" checks. This line is printed if the
    user manages to do it.
    """
    print("You managed to not leave the computer any moves.")
    print("The game will continue without the computer moving, " +
          "you're going to lose anyway.")
    print()
